package test.mel.oop;

public class Dog {
    public String name;
    public int age;
    public String breed;

    public Dog(String name, int i, String breed) {
        this.name = name;
        this.age = i;
        this.breed = breed;
    }

    public Dog() {
        this.name = "Fido";
        this.age = 2;
        this.breed = "corgi";
    }

    public void greeting() {
        System.out.println("Hello ! Je m'appelle " + name + " j'ai " + age + " ans et je suis un beau "
                + breed + " Wouf ! ;-)");
    }

    public void bark(String something) {
        System.out.println("Woof sur " + something);
    }
}
