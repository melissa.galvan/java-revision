package test.mel.oop;

public class Validator {

    private int minLength;
    private int maxLength;

    public Validator(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public boolean isCorrectLength(String word) {
        if (word.length() > minLength && word.length() < maxLength) {
            return true;
        }
        return false;
    }

    public boolean haveTrailingSpaces(String word) {
        if ((word.startsWith(" ")) || (word.endsWith(" "))) {
            return true;
        }
        return false;
    }

    public boolean haveSpecialChars(String word) {
        if ((word.contains("_")) || (word.contains("-")) || (word.contains("!"))) {
            return true;
        }
        return false;
    }

    public boolean validate(String word) {
        if (isCorrectLength(word) && !haveTrailingSpaces(word) && !haveSpecialChars(word)) {
            return true;
        }
        return false;
    }
}
